console.log("Hello World");

/*
	try to store the following grades inside a variable and log the variable in the console.
		98.5
		94.3
		89.2
		90.1

	send an output in the google chat
*/
// Typically, we would have coded it like this
/*let grade1 = 98.5;
let grade2 = 94.3;
let grade3 = 89.2;
let grade4 = 90.1;

console.log(grade1);
console.log(grade2);
console.log(grade3);
console.log(grade4);*/

// but we could actually do it like this using arrays.
/*
	Array
		Arrays are used to store multiple related values in a single variable
		They are declared using square brackets([]) also known as "Array Literals"
		Arrays also provide access to a number of functions/methods that help in achieving tasks that we can perform on the elements inside the arrays

		Arrays are used to sotre numerous amouns of data to manipulate in order perform methods
		Methods are used similar to functions associated with objects
		
		Arrays are also referred to as objects which is another data type.
		The only difference between the two is that arrays contant information in a form of list, but an object uses "properties" and "values" in its elements

		SYNTAX:
			let/const arrayName = [ elementA, elementB, elementC, ..., elementN ] 
*/
let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades);
// it is important that we store RELATED values inside an array so that its variable can live up to its description of its value
let computerBrands = ["Lenovo", "Dell", "Asus", "HP", "MSI", "Acer", "CDR-King"];
console.log(computerBrands);
// this is possible but, since we only provide list in arrays, this method of writing is not recommended
let mixedArr = [12, "Asus", null, undefined];
console.log(mixedArr);

// Reassigning of array values
console.log("Array before reassigning: ");
console.log(mixedArr);
// accessing the elements requires the arrayName + index enclosed in square brackets
// this can also be done in const arrays
mixedArr[0] = "Hello World";
console.log("Array after reassigning: ");
console.log(mixedArr);

// SECTION - Reading from Arrays
/*
	accessing arraya elements is one of the more common taskss that we do with an array
	this can be done through the use of array indeces
		index - term used to declare the position of an element in an array.
		in JS, the first element is associated with number 0, incrementing as the number of the elements increase.
		SYNTAX:
			ArrayName[index];
*/
console.log(grades);

console.log(grades[0]);
console.log(computerBrands[3]);
console.log(computerBrands[6]);
// how we can get the number of elements in an array
console.log(computerBrands.length);
// accessing an index that is not existing would return undefined because this means that the element is not there
console.log(computerBrands[7]);

// One of the common use for the .length property
if (computerBrands.length > 5) {
    console.log("We have too many suppliers. Please coordinate with the operations manager.");
}

// Since the the first element of an array starts at 0, subtracting 1 from the length will offset the value by one allowing us to get the last index of the array, in case we forgot the number of its element
let lastElementIndex = computerBrands.length - 1;
console.log(computerBrands[lastElementIndex]);

// Section - ARRAY METHODS
// methods are similar to functions, these array methods can be done in array and objects alone
// Mutator Methods
/*
	- Mutator Methods - are functions that "mutate" or change/alter an array after they have been created
	- These methods manipulate the original array performing various tasks such as adding and removing elements
*/
let fruits = ["Apple", "Mango", "Rambutan", "Lanzones", "Durian"];
console.log(fruits);


// push() - adds an element at the end of the array
/*
	SYNTAX: 
		arrayName.push()
*/
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);

console.log("Mutated Array from Push Method:");
console.log(fruits);

/*
fruits[6] = "Orange"
console.log(fruits);

the code above can also be written like this:*/
fruits.push("Orange");
// it can also accept multiple arguments
fruits.push("Guava", "Avocado");
console.log("Mutated Array from Push Method:");
console.log(fruits);

// pop() - remove an element from the end of an array
/*
	SYNTAX: 
		arrayName.pop()
*/
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from Pop Method:");
console.log(fruits);

fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from Pop Method:");
console.log(fruits);

// unshift - adds an element at the start of an array; 
/*
	arrayName.unshift(element);
*/

fruits.unshift("Lime", "Banana");
/*
	separating two unshifts will result into the second element being at the index 0 of the array
	fruits.unshift("Banana");
*/
console.log("Mutated Array from Unshift Method:");
console.log(fruits);

// shift - removes an element from the start of the array
/*
	SYNTAX:
		arrayName.shift();

*/
let fruitRemoved = fruits.shift();
console.log("Mutated Array from Shift Method:");
console.log(fruits);

fruits.shift();
console.log("Mutated Array from Shift Method:");
console.log(fruits);

// splice - simultaneously removes and adds elements from specified index number.
/*
	SYNTAX:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(2, 2, "Lime", "Cherry", "Strawberry");
console.log("Mutated Array from Splice Method:");
console.log(fruits);

// sort - rearranges the elements in an array in alphanumeric order
/*
	SYNTAX:
		arrayName.sort();
*/
fruits.sort();
console.log("Mutated Array from Sort Method:");
console.log(fruits);

// reverse - reverses the order of the array
/*
	SYNTAX:
		arrayName.reverse();
*/
fruits.reverse();
console.log("Mutated Array from Reverse Method:");
console.log(fruits);

// NON-Mutator Methods
/*
	-are functions that do not modify the original array
	-these methods do not manipulate the elements inside the array even they are performing tasks such as returning elements from an array and combining them with other arrays and printing the output
*/

let countries = ["RUS", "CH", "JPN", "PH", "USA", "KOR", "AUS", "CAN", "PH"];
// indexOf() 
/*
	-returns the index of the first matching element found in an array
	-the search process will start from the first element down to the last
	-SYNTAX: 
		arrayName.indexOf(searchValue);
*/
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf: " + firstIndex);
firstIndex = countries.indexOf("KAZ");
// returns -1 for non-existing elements
console.log("Result of indexOf: " + firstIndex);

// lastIndexOf() - starts the search process from the last element down to first
/*
	SYNTAX
		arrayName.lastIndexOf(searchValue);
*/
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf: " + lastIndex);
lastIndex = countries.lastIndexOf("KAZ");
console.log("Result of lastIndexOf: " + lastIndex);

// slice
/*
	copies a part of the array and returns a new array
	SYNTAX:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingElement);
*/
let slicedArrayA = countries.slice(2);
console.log("Result of Slice: " + slicedArrayA);
console.log(countries);

// slicing the index 4 up until the 7th element
let slicedArrayB = countries.slice(4, 7);
console.log("Result of Slice: " + slicedArrayB);
console.log(countries);

// slicing off the elements from the end of the array
let slicedArrayC = countries.slice(-3);
console.log("Result of Slice: " + slicedArrayC);
console.log(countries);

// toString() - returns a new array as a string separated by commas
/*
	SYNTAX:
		arrayName.toString();
*/
let stringArray = countries.toString();
console.log("Result of toString: ");
console.log(stringArray);

// concat() - used to combine two arrays and returns the combined result
/*
	SYNTAX:
		arrayA.concat(arrayB);
		arrayA.concat(arrayB, arrayC);
*/
let tasksA = ["drink HTML", "eat Javascript"];
let tasksB = ["inhale CSS", "breathe SASS"];
let tasksC = ["get GIT", "be node"];

let tasks = tasksA.concat(tasksB);
console.log("Result of concat: ");
console.log(tasks);

// combining multiple arrays
let allTasks = tasksA.concat(tasksB, tasksC);
console.log("Result of concat: ");
console.log(allTasks);

// join() - returns an array as string separated by specified string separator
/*
	SYNTAX:
		arrayName.join('stringSeparator');
*/
let users = ["John", "Jane", "Joe", "Jobert", "Julius"];

console.log(users.join());
console.log(users.join(' '));
console.log(users.join('-'));

// Iteration methods
/*
	- iteration methods are loops designed to perform repetitive tasks on arrays
	- useful for manipulating array data resulting in complex tasks
*/

//forEach
/*
	-similar to a for loop that loops through all elements
	- variable names for arrays are usually written in plural form of the data stored in an array
	- it's common practice to use the  singular form of the array content for parameter names used in array loops
	-array iterations normally work with a function suplied as an argument
	-how these function works is by performing tasks that are predefined within the array's method
	SYNTAX:
		arrayName.forEach(function(individualElement){
			statement/s
		})
*/
allTasks.forEach(function(task) {
        console.log(task);
    })
    // forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task) {
    if (task.length > 10) {
        // we stored the filtered elements inside another variable to avoid confusion should we need the original array in tact
        filteredTasks.push(task);
    }
})

console.log("Result of forEach: ");
console.log(filteredTasks);

// map
/*
	- iterates on each element AND returns a new array with different values depending on the result of the function's operation.
	- this is useful for performing tasks where  mutating/changing the elements are required
	SYNTAX:
		let/const resultArray = arrayName.map(function(individualElement){
			return statement
		})
*/
let numbers = [1, 2, 3, 4, 5];

let numbersMap = numbers.map(function(number) {
    // unlike forEach, return statement is needed in map method to create a value that is stored in another array 
    return number * number;
})
console.log("Result of map: ");
console.log(numbersMap);

// every()
/*
	- checks if ALL elements pass the given condition
	- returns a boolean data type depending if all elements meet the codition (true) or not (false)
	SYNTAX:
		let/const resultName = arrayName.every(function(individualElement){
			return expression/condition
		})
*/
let allValid = numbers.every(function(number) {
    return (number < 3);
})
console.log("Result of every: ");
console.log(allValid);
console.log(numbers);

// some()
/*
	- checks if atleast one element in the array passes the given condition
	- returns boolean depending on the result of the function
	SYNTAX:
		let/const resultName = arrayName.some(function(individualElement){
			return expression/condition
		})
*/
let someValid = numbers.some(function(number) {
    return (number < 2);
})
console.log("Result of some: ");
console.log(someValid);
console.log(numbers);

// filter
/*
	- returns a new array that contains copies of the elements which meet the given condition
	- returns empty array if there are no elements that meet the condition
	- useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration method such as forEach + if statement + push in an earlier example
			- it is important that we make our work as efficient as possible.
	SYNTAX: 
		let/const resultName = arrayName.filter(function(individualElement){
			return expression/condition
		})
*/
let filterValid = numbers.filter(function(number) {
    return (number < 3);
})
console.log("Result of filter: ");
console.log(filterValid);
console.log(numbers);